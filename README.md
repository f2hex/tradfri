# Dockerized COAP client

## IKEA Trädfri - a IPSO based smart lightning system



The IKEA Trädfri system makes use of the IPSO model
([IP for Smart Objects](https://www.ipso-alliance.org/)). You can find
more details about IPSO Common Object model
[here](https://github.com/IPSO-Alliance/pub/blob/master/README.md)
while the IPSO object set is described
[here](https://github.com/IPSO-Alliance/pub/blob/master/reg/README.md).
IPSO model makes use of the
[COAP](https://tools.ietf.org/html/rfc7252) - Constrained Application
Protocol

## Implementation of a Docker image for the COAP client

A Docker based COAP client (container to be used as a CLI) can be
built using the great work already done by several developers in the
context of the Trädfri investigation and exploration. In particular
the COAP library adapation for Trafri. Here are some useful links:

* [Home Assistant: IKEA Trädfri Gateway Zigbee](https://community.home-assistant.io/t/ikea-tradfri-gateway-zigbee/14788/12)
* [COAP Endpoints on IKEA Trädfri Gateway](https://bitsex.net/software/2017/coap-endpoints-on-ikea-tradfri/)
* [A CoAP (RFC 7252) implementation in C](https://github.com/obgm/libcoap)

## How to build the Docker image

The `build` simple script it is used to create a single executable
container with the `coap-client`command in it using a builder pattern
approach.  The base image creation script can be found
[here](https://gitlab.com/arm_project/f2hex/tree/master/alpine).

The resulting container image can be used to execute commands against
the IKEA Trädfri gateway. 

### Some details about the build process implemented in the `Dockerfile-arm64-dev`

Usually the `libcoap` should build correctly on standard Linux distros
(like Ubuntu and Debian) however because I chose to use Alpine Linux I
had to overcome some issues. More specifically:

1. Errors due to undefined basic type constants, like `unknown type name 'u_int32_t'`
2. Debug output produced by the `coap-client` - this can interfere with other software that could use `coap-client` so any debug output must be removed
3. Errors du to missing time types related constants definition, like `error: unknown type name 'time_t'`

The 1st and 2nd issues mentioned above issues were solved by
pre-defining the `CFLAG` environment variable in this way: `export
CFLAGS="-DSHA2_USE_INTTYPES_H -DNDEBUG"` before running running the
`./configure` command. The 3rd issue was resolved by a patch to
the `include/coap/libcoap.h` source file (patch file is in the repo).
Maybe updated version of the `libcoap` could not need these fixes.

**Note**: I made the build on an ARM 64 bit board, specifically the [Orange
Pi PC2](https://www.armbian.com/orange-pi-pc2/) with `Armbian Linux - 5.27.170429 nightly Ubuntu 16.04.2 LTS
4.10.0-sun50iw2`. The used Docker version is: `1.12.6, build 78d1802`.

## Testing the docker image

After having built the container image you can test it with a first by issuing basic command to the Trädfri gateway.

### Querying for endpoints

Here is the command asking for querying the gateway available endpoints:

```
docker run f2hex/arm64-coap-client \
  -k "GATEWAY-SECURITY-KEY" \
  -v 0 \
  -m get "coaps://192.168.2.13:5684/.well-known/core" 
```

You should get something like this:
```
<//15001/65539>;ct=0;obs,<//15001/65542>;ct=0;obs,<//15001/65543>;ct=0;obs,<//15001/65540>;ct=0;obs,<//15001/65541>;ct=0;obs,<//15001/65536>;ct=0;obs,<//15001/65537>;ct=0;obs,<//15001/65538>;ct=0;obs,<//15004/157785>;ct=0;obs,<//15004/179489>;ct=0;obs,<//15004/151882>;ct=0;obs,<//15005/157785/217474>;ct=0;obs,<//15005/157785/209912>;ct=0;obs,<//15005/157785/213555>;ct=0;obs,<//15005/157785>;ct=0;obs,<//15005/179489/207261>;ct=0;obs,<//15005/179489/204418>;ct=0;obs,<//15005/179489/225636>;ct=0;obs,<//15005/179489>;ct=0;obs,<//15005/151882>;ct=0;obs,<//15005/151882/217737>;ct=0;obs,<//15005/151882/213551>;ct=0;obs,<//15005/151882/209389>;ct=0;obs,<//15001>;ct=0;obs,<//15001/reset>;ct=0,<//status>;ct=0;obs,<//15005>;ct=0;obs,<//15004>;ct=0;obs,<//15004/add>;ct=0,<//15004/remove>;ct=0,<//15006>;ct=0;obs,<//15011/15012>;ct=0;obs,<//15011/9034>;ct=0,<//15011/9030>;ct=0,<//15011/9031>;ct=0,<//15011/9063>;ct=0,<//15011/9033>;ct=0,<//15010>;ct=0;obs,<//15004/178073>;ct=0;obs,<//15005/178073>;ct=0;obs,<//15005/178073/214963>;ct=0;obs,<//15005/178073/225534>;ct=0;obs,<//15005/178073/219317>;ct=0;obs,<//15010/298971>;ct=0;obs,<//15010/305763>;ct=0;obs
```

### Turning on or off the first smart bulb

To turn on or off the first bulb just run these commands.

Turn on:
```
docker run f2hex/arm64-coap-client \
  -k "GATEWAY-SECURITY-KEY" \
  -v 0 \
  -m put \
  -e '{ "3311": [{ "5850": 1 }] }' "coaps://192.168.2.13:5684/15001/65537"
```
Turn off:
```
docker run f2hex/arm64-coap-client \
  -k "GATEWAY-SECURITY-KEY" \
  -v 0 -m put \
  -e '{ "3311": [{ "5850": 0 }] }' "coaps://192.168.2.13:5684/15001/65537"
```


